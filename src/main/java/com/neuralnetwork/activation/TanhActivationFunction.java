package com.neuralnetwork.activation;

public class TanhActivationFunction implements ActivationFunction {
    private double slope = 1d;

    @Override
    public double activate(double input) {
        final double eToX = Math.exp(2 * slope * input);
        final double tanh = (eToX - 1d) / (eToX + 1d);
        return tanh;
    }

    @Override
    public double derivative(double value) {
        // 1- tanh^2(x)
        final double tanh = activate(value);
        return 1 - (tanh * tanh);
    }
}
