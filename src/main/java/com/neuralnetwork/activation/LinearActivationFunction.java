package com.neuralnetwork.activation;

/**
 * Created by jamie on 08/01/18.
 */
public class LinearActivationFunction implements ActivationFunction {
    @Override
    public double activate(double input) {
        return input;
    }

    @Override
    public double derivative(double value) {
        return 1;
    }
}
