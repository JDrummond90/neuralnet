package com.neuralnetwork.activation;

/**
 * Created by jamie on 08/01/18.
 */
public interface ActivationFunction {
    double activate(double input);

    double derivative(double value);
}
