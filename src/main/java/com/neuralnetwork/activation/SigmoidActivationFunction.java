package com.neuralnetwork.activation;

/**
 * Created by jamie on 08/01/18.
 */
public class SigmoidActivationFunction implements ActivationFunction {

    private double slope = 1d;

    public double activate(double input){
        return 1d/(1d+Math.exp(-slope *input));
    }

    public double derivative(double value){
        double point = activate(value);
        return slope * point * (1d - point);
    }
}
