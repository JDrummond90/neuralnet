package com.neuralnetwork.activation;

public class LeakyRectifiedLinearActivationFunction implements ActivationFunction {

    private static final double LEAK = 0.01;
    private static final double MAX = 500;

    @Override
    public double activate(double input) {
        return input > 0 ? Math.min(input, MAX) : LEAK * input;
    }

    @Override
    public double derivative(double value) {
        return value > 0 ? 1 : LEAK;
    }
}
