package com.neuralnetwork.activation;

/**
 * ReLU
 */
public class RectifiedLinearActivationFunction implements ActivationFunction {
    @Override
    public double activate(double input) {
        return Math.max(0, input);
    }

    @Override
    public double derivative(double value) {
        return value > 0 ? 1 : 0;
    }
}
