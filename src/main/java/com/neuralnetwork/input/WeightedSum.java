package com.neuralnetwork.input;

import com.neuralnetwork.Connection;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class WeightedSum implements InputFunction {
    public double calculateInput(List<Connection> inputConnections) {
        double input = 0;
        for (int i = 0; i < inputConnections.size(); i++) {
            Connection inputConnection = inputConnections.get(i);
            input += inputConnection.weightedInput();
        }
        return input;
    }
}
