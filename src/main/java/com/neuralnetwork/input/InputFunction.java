package com.neuralnetwork.input;

import com.neuralnetwork.Connection;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public interface InputFunction {
    double calculateInput(List<Connection> inputConnections);
}
