package com.neuralnetwork;

import com.neuralnetwork.activation.ActivationFunction;
import com.neuralnetwork.input.InputFunction;
import com.neuralnetwork.utility.GuardUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jamie on 08/01/18.
 */
public class Network {

    public static class NetworkBuilder {

        private List<Layer> layers = new ArrayList<>();

        public NetworkBuilder() {
        }

        public List<Layer> getLayers() {
            return layers;
        }

        public void addLayers(ActivationFunction activationFunction, InputFunction inputFunction, WeightInitialiser weightInitialiser, boolean useBias, int... neuronCounts) {
            assert neuronCounts.length >= 3;
            for (int i = 0; i < neuronCounts.length; i++) {
                int neuronCount = neuronCounts[i];
                if (i == 0) {
                    addInputLayer(neuronCount, useBias);
                } else if (i == neuronCounts.length - 1) {
                    addOutputLayer(neuronCount, activationFunction, inputFunction, weightInitialiser);
                } else {
                    addHiddenLayer(neuronCount, useBias, activationFunction, inputFunction, weightInitialiser);
                }
            }
        }

        public void wireInNewLayer(Layer layer, WeightInitialiser weightInitialiser) {
            if (layers.size() > 0) {
                wireUpLayer(layers.get(layers.size() - 1), layer, weightInitialiser);
            }
            layers.add(layer);
        }

        public Layer addInputLayer(int neuronCount, boolean useBias) {
            List<Neuron> neurons = createInputNeurons(neuronCount, useBias);
            InputLayer inputLayer = new InputLayer(neurons);
            layers.add(inputLayer);
            return inputLayer;
        }

        public Layer addHiddenLayer(int neuronCount, boolean useBias, ActivationFunction activationFunction, InputFunction inputFunction, WeightInitialiser weightInitialiser) {
            List<Neuron> neurons = createNeurons(neuronCount, useBias, activationFunction, inputFunction);
            Layer hiddenLayer = new HiddenLayer(neurons);
            wireInNewLayer(hiddenLayer, weightInitialiser);
            return hiddenLayer;
        }

        public Layer addOutputLayer(int neuronCount, ActivationFunction activationFunction, InputFunction inputFunction, WeightInitialiser weightInitialiser) {
            Layer outputLayer = new OutputLayer(createNeurons(neuronCount, false, activationFunction, inputFunction));
            wireInNewLayer(outputLayer, weightInitialiser);
            return outputLayer;
        }

        public List<Neuron> createInputNeurons(int neuronCount, boolean useBias) {
            List<Neuron> neurons = new ArrayList<>();
            for (int i = 0; i < neuronCount; i++) {
                Neuron neuron = new InputNeuron();
                neurons.add(neuron);
            }
            if (useBias) {
                neurons.add(new BiasNeuron());
            }
            return neurons;
        }

        public List<Neuron> createNeurons(int neuronCount, boolean useBias, ActivationFunction activationFunction, InputFunction inputFunction) {
            List<Neuron> neurons = new ArrayList<>();
            for (int i = 0; i < neuronCount; i++) {
                Neuron neuron = new AutoNeuron(activationFunction, inputFunction);
                neurons.add(neuron);
            }
            if (useBias) {
                neurons.add(new BiasNeuron());
            }
            return neurons;
        }

        private void wireUpLayer(Layer priorLayer, Layer nextLayer, WeightInitialiser weightInitialiser) {
            for (Neuron from : priorLayer.getNeurons()) {
                for (Neuron to : nextLayer.getNeurons()) {
                    if (!(to instanceof BiasNeuron)) {
                        Connection.addIfMissing(from, to);
                    }
                }
            }
            weightInitialiser.initialise(nextLayer.getNeurons());
        }

        public Network build() {
            return new Network(layers);
        }

    }

    public static NetworkBuilder builder() {
        return new NetworkBuilder();
    }

    private final List<Layer> allLayers;
    private final List<Layer> hiddenLayers;
    private final Layer inputLayer;
    private final Layer outputLayer;
    private final List<Neuron> inputNeurons;
    private final double[] output;

    private Network(List<Layer> allLayers) {
        this.allLayers = allLayers;
        this.inputLayer = allLayers.get(0);
        this.outputLayer = allLayers.get(allLayers.size() - 1);
        this.hiddenLayers = allLayers.subList(1, allLayers.size() - 1);
        inputNeurons = inputLayer.getNeurons().stream().filter(x -> x instanceof InputNeuron).collect(Collectors.toList());
        output = new double[outputLayer.getNeurons().size()];
    }

    public double[] calculate(double[] input) {
        inputData(input);
        for (int i = 0; i < allLayers.size(); i++) {
            Layer layer = allLayers.get(i);
            layer.calculate();
        }
        return calculateResult();
    }

    private void inputData(double[] input) {
        // TODO improve
        //assert dataRow.length == inputLayer.getNeurons().size();
        for (int i = 0; i < inputNeurons.size(); i++) {
            InputNeuron neuron = (InputNeuron) inputNeurons.get(i);
            neuron.setInput(input[i]);
        }
    }

    private double[] calculateResult() {
        // TODO improve
        List<Neuron> neurons = outputLayer.getNeurons();
        for (int i = 0; i < neurons.size(); i++) {
            output[i] = neurons.get(i).getOutput();
        }
        return output;
    }

    public List<Layer> getAllLayers() {
        return allLayers;
    }

    public Layer getInputLayer() {
        return inputLayer;
    }

    public List<Layer> getHiddenLayers() {
        return hiddenLayers;
    }

    public Layer getOutputLayer() {
        return outputLayer;
    }
}
