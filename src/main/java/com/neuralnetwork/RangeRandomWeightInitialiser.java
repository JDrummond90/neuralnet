package com.neuralnetwork;

import java.util.List;
import java.util.Random;

public class RangeRandomWeightInitialiser implements WeightInitialiser {

    private final Random random;
    private final double min;
    private final double max;

    public RangeRandomWeightInitialiser(double min, double max) {
        this.random = new Random();
        this.min = min;
        this.max = max;
    }

    @Override
    public void initialise(List<Neuron> neurons) {
        for (Neuron neuron : neurons) {
            for (Connection connection : neuron.getInputConnections()) {
                connection.setWeight(min + random.nextDouble() * (max - min));
            }
        }
    }
}
