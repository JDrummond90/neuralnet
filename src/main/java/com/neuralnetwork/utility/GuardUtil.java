package com.neuralnetwork.utility;

public class GuardUtil {

    public static void isFinite(double value, String error) {
        if (!Double.isFinite(value)){
            throw new IllegalStateException(error);
        }
    }
}
