package com.neuralnetwork;

import java.util.List;

public class SoftmaxLayer implements Layer {

    private final List<Neuron> neurons;
    private final double[] exponents;

    public SoftmaxLayer(List<Neuron> neurons) {
        this.neurons = neurons;
        this.exponents = new double[neurons.size()];
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }

    @Override
    public void calculate() {
        double total = 0;
        for (int i = 0; i < neurons.size(); i++) {
            Neuron neuron = neurons.get(i);
            neuron.calculateInput();
            exponents[i] = Math.exp(neuron.getInput());
            total += exponents[i];
        }

        for (int i = 0; i < neurons.size(); i++) {
            Neuron neuron = neurons.get(i);
            if(total == 0) {
                neuron.setOutput(0);
            } else {
                double v = exponents[i] / total;
                neuron.setOutput(v);
            }
        }
    }
}
