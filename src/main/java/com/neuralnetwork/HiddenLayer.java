package com.neuralnetwork;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class HiddenLayer implements Layer {
    private final List<Neuron> neurons;

    public HiddenLayer(List<Neuron> neurons) {

        this.neurons = neurons;
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }
}
