package com.neuralnetwork;

import java.util.List;
import java.util.Random;

/**
 * Created by jamie on 08/01/18.
 */
public class RandomWeightInitialiser implements WeightInitialiser {

    private final Random random = new Random();

    public void initialise(List<Neuron> neurons) {
        for (Neuron neuron : neurons) {
            for (Connection connection : neuron.getInputConnections()) {
                connection.setWeight(random.nextDouble() - 0.5);
            }
        }
    }
}
