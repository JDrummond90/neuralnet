package com.neuralnetwork;

/**
 * Created by jamie on 08/01/18.
 */
public class Connection {
    private final Neuron from;
    private final Neuron to;
    private double weight;

    public Connection(Neuron from, Neuron to) {
        this(from, to, 0d);
    }

    public Connection(Neuron from, Neuron to, double initialWeight) {
        this.from = from;
        this.to = to;
        this.weight = initialWeight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Neuron getFrom() {
        return from;
    }

    public Neuron getTo() {
        return to;
    }

    public double weightedInput() {
        return weight * from.getOutput();
    }

    public static Connection addIfMissing(Neuron from, Neuron to) {
        return addIfMissing(from, to, 0d);
    }

    public static Connection addIfMissing(Neuron from, Neuron to, double initialWeight) {
        if (from == to) {
            throw new IllegalStateException("Unable to add connection");
        }

        Connection output = null, input = null;
        for (Connection outputConnection : from.getOutputConnections()) {
            if (outputConnection.getTo() == to) {
                output = outputConnection;
            }
        }
        for (Connection inputConnection : to.getInputConnections()) {
            if (inputConnection.getFrom() == from) {
                input = inputConnection;
            }
        }

        if (input == null && output == null) {
            Connection connection = new Connection(from, to, initialWeight);
            from.getOutputConnections().add(connection);
            to.getInputConnections().add(connection);
            return connection;
        } else if (input == output) {
            return input;
        } else {
            throw new IllegalStateException("Connection exists in either input or output but not both");
        }
    }

    @Override
    public String toString() {
        return "Connection{" +
                "from=" + from +
                ", to=" + to +
                ", weight=" + weight +
                '}';
    }
}
