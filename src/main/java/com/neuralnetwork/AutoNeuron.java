package com.neuralnetwork;

import com.neuralnetwork.activation.ActivationFunction;
import com.neuralnetwork.input.InputFunction;
import com.neuralnetwork.utility.GuardUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class AutoNeuron implements Neuron {

    private final List<Connection> inputConnections;
    private final List<Connection> outputConnections;
    private final ActivationFunction activationFunction;
    private final InputFunction inputFunction;

    private double error;
    private double input;
    private double output;

    public AutoNeuron(ActivationFunction activationFunction, InputFunction inputFunction) {
        this.activationFunction = activationFunction;
        this.inputFunction = inputFunction;
        this.inputConnections = new ArrayList<>();
        this.outputConnections = new ArrayList<>();
    }

    @Override
    public List<Connection> getInputConnections() {
        return inputConnections;
    }

    @Override
    public List<Connection> getOutputConnections() {
        return outputConnections;
    }

    @Override
    public double getError() {
        return error;
    }

    @Override
    public void setError(double error) {
        this.error = error;
    }

    @Override
    public double getInput() {
        return input;
    }

    @Override
    public void calculateInput() {
        input = inputFunction.calculateInput(inputConnections);
        GuardUtil.isFinite(input, "Neuron input is not finite");
    }

    @Override
    public void calculateOutput() {
        output = activationFunction.activate(input);
        GuardUtil.isFinite(input, "Neuron output is not finite");
    }

    @Override
    public double getOutput() {
        return output;
    }

    @Override
    public double getDerivative() {
        return activationFunction.derivative(input);
    }

    // TODO used by softmax, should move neuron instantiation into layer logic
    public void setOutput(double output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return "AutoNeuron{" +
                "error=" + error +
                ", input=" + input +
                ", output=" + output +
                '}';
    }
}
