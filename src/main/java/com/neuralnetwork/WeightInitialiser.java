package com.neuralnetwork;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public interface WeightInitialiser {

    void initialise(List<Neuron> neurons);
}
