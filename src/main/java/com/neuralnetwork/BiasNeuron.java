package com.neuralnetwork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BiasNeuron implements Neuron {
    private final List<Connection> outputConnections = new ArrayList<>();

    @Override
    public List<Connection> getInputConnections() {
        return Collections.emptyList();
    }

    @Override
    public List<Connection> getOutputConnections() {
        return outputConnections;
    }

    @Override
    public double getError() {
        return 0;
    }

    @Override
    public void setError(double error) {

    }

    @Override
    public double getInput() {
        return 0;
    }

    @Override
    public void calculateInput() {

    }

    @Override
    public void calculateOutput() {

    }

    @Override
    public double getOutput() {
        return 1;
    }

    @Override
    public void setOutput(double v) {

    }

    @Override
    public double getDerivative() {
        return 1;
    }

    @Override
    public String toString() {
        return "BiasNeuron{}";
    }
}
