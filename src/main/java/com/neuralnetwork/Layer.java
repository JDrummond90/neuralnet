package com.neuralnetwork;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public interface Layer {

    List<Neuron> getNeurons();

    default void calculate() {
        List<Neuron> neurons = getNeurons();
        for (int i = 0; i < neurons.size(); i++) {
            Neuron neuron = neurons.get(i);
            neuron.calculateInput();
            neuron.calculateOutput();
        }
    }
}
