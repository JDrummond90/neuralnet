package com.neuralnetwork;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class OutputLayer implements Layer {
    private List<Neuron> neurons;

    public OutputLayer(List<Neuron> neurons) {
        this.neurons = neurons;
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }
}
