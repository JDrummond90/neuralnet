package com.neuralnetwork;

import com.neuralnetwork.activation.ActivationFunction;
import com.neuralnetwork.input.InputFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class InputNeuron implements Neuron {
    private final List<Connection> inputConnections;
    private final List<Connection> outputConnections;

    private double input;

    public InputNeuron(){
        this.inputConnections = new ArrayList<>();
        this.outputConnections = new ArrayList<>();
    }

    void setInput(double input) {
        this.input = input;
    }

    @Override
    public List<Connection> getInputConnections() {
        return inputConnections;
    }

    @Override
    public List<Connection> getOutputConnections() {
        return outputConnections;
    }

    @Override
    public double getError() {
        return 0;
    }

    @Override
    public void setError(double error) {
        // TODO, is this an exception/should this class have these methods
    }

    @Override
    public double getInput() {
        return input;
    }

    @Override
    public void calculateInput() {

    }

    @Override
    public void calculateOutput() {

    }

    @Override
    public double getOutput() {
        // Input Neuron simply passes along the value
        return input;
    }

    @Override
    public void setOutput(double v) {
    }

    @Override
    public double getDerivative() {
        return 1;
    }

    @Override
    public String toString() {
        return "InputNeuron{" +
                "input=" + input +
                '}';
    }
}
