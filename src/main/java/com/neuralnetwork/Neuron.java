package com.neuralnetwork;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 *
 * TODO layers should create neurons and this interface should be cut down
 */
public interface Neuron {

    List<Connection> getInputConnections();

    List<Connection> getOutputConnections();

    double getError();

    void setError(double error);

    /**
     * Input to the neuron, sometimes referred to as net input
     */
    double getInput();

    void calculateInput();

    void calculateOutput();

    double getOutput();

    void setOutput(double v);

    double getDerivative();
}
