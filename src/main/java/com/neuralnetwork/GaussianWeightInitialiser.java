package com.neuralnetwork;

import java.util.List;
import java.util.Random;

public class GaussianWeightInitialiser implements WeightInitialiser {

    private final Random random = new Random();
    private final double mean;
    private final double stdDev;

    public GaussianWeightInitialiser(double mean, double stdDev) {
        this.mean = mean;
        this.stdDev = stdDev;
    }

    @Override
    public void initialise(List<Neuron> neurons) {
        for (Neuron neuron : neurons) {
            for (Connection connection : neuron.getInputConnections()) {
                connection.setWeight(random.nextGaussian() * stdDev + mean);
            }
        }
    }
}
