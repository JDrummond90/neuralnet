package com.neuralnetwork.training;

import com.neuralnetwork.*;
import com.neuralnetwork.training.error.ErrorFunction;
import com.neuralnetwork.training.rule.MaxEpochs;
import com.neuralnetwork.training.rule.StopRule;
import com.neuralnetwork.utility.GuardUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jamie on 08/01/18.
 */
public class BackPropagation {

    private static final Logger LOG = LogManager.getLogger(BackPropagation.class);

    private final Network network;
    private final ErrorFunction errorFunction;

    private final List<StopRule> stopRules = new ArrayList<>();

    private final double learningRate;
    private final double maxError;

    public BackPropagation(Network network, ErrorFunction errorFunction,
                           double learningRate, double maxError) {
        this.network = network;
        this.errorFunction = errorFunction;
        this.learningRate = learningRate;
        this.maxError = maxError;
    }

    public void addRule(StopRule stopRule) {
        stopRules.add(stopRule);
    }

    public void withDefaultRules() {
        //stopRules.add(new RateOfLearningNotImproving(0.0001, 0.9999));
        stopRules.add(new MaxEpochs(1000000));
    }

    public void learn(DataSet dataSet) {
        long startTimeMs = System.currentTimeMillis();
        int epochs = 0;
        double lastError;
        boolean canProceed;
        do {
            epochs++;
            lastError = runEpoch(dataSet);
            if (epochs < 10 || epochs % 1000 == 0) {
                LOG.info("Epochs: {}, Error: {}", epochs, lastError);
                System.out.println("epoch per ms= " + (double) epochs / (System.currentTimeMillis() - startTimeMs));
            }
            canProceed = canProceed(lastError, epochs);
        } while (lastError > maxError && canProceed);

        LOG.info("Epochs: {}, Error: {}", epochs, lastError);

        if (!canProceed) {
            for (StopRule stopRule : stopRules) {
                if (!stopRule.canProceed(lastError, epochs)) {
                    LOG.warn(stopRule.getStopMessage());
                }
            }
        } else {
            LOG.info("Converged after {} iterations", epochs);
        }
    }

    private double runEpoch(DataSet dataSet) {
        errorFunction.startEpoch();
        for (DataRow dataRow : dataSet.getRows()) {
            final double[] outputs = network.calculate(dataRow.getInput());
            updateWeightsFromError(dataRow.getExpectedOutput(), outputs);
        }

        return errorFunction.totalError();
    }

    private boolean canProceed(double lastError, int iterations) {
        for (StopRule stopRule : stopRules) {
            if (!stopRule.canProceed(lastError, iterations)) {
                return false;
            }
        }
        return true;
    }

    private void updateWeightsFromError(double[] expectedOutput, double[] outputs) {
        final double[] error = errorFunction.errorPerOutput(outputs, expectedOutput);
        updateOutputLayer(network.getOutputLayer(), error);

        for (int i = network.getHiddenLayers().size() - 1; i >= 0; i--) {
            updateHiddenLayer(network.getHiddenLayers().get(i));
        }
    }

    private void updateOutputLayer(Layer layer, double[] error) {
        List<Neuron> neurons = layer.getNeurons();
        for (int i = 0; i < neurons.size(); i++) {
            Neuron neuron = neurons.get(i);

            if (error[i] == 0) {
                neuron.setError(0);
            } else {
                final double delta = error[i] * neuron.getDerivative();
                updateNeuronWeights(neuron, delta);
            }
        }
    }

    /**
     * Using chain rule partial derivatives iterate backwards through each layer
     * applying the loss of the prior layer
     * @param layer
     */
    private void updateHiddenLayer(Layer layer) {
        List<Neuron> neurons = layer.getNeurons();
        for (int i = 0; i < neurons.size(); i++) {
            Neuron neuron = neurons.get(i);
            final double totalDelta = getTotalDelta(neuron);
            final double f1 = neuron.getDerivative();
            final double delta = f1 * totalDelta;
            updateNeuronWeights(neuron, delta);
        }
    }

    private double getTotalDelta(Neuron neuron) {
        double totalDelta = 0;
        List<Connection> outputConnections = neuron.getOutputConnections();
        for (int i = 0; i < outputConnections.size(); i++) {
            Connection connection = outputConnections.get(i);
            final double delta = connection.getTo().getError() * connection.getWeight();
            totalDelta += delta;
        }
        return totalDelta;
    }

    private void updateNeuronWeights(Neuron neuron, double delta) {
        neuron.setError(delta);

        List<Connection> inputConnections = neuron.getInputConnections();
        for (int i = 0; i < inputConnections.size(); i++) {
            Connection connection = inputConnections.get(i);
            final double output = connection.getFrom().getOutput();
            final double weightChange = -learningRate * delta * output;
            // TODO when to apply changes, after a set of data or after each piece of data (batch vs online)
            double newWeight = connection.getWeight() + weightChange;
            if(newWeight > 10000) {
                newWeight = 10000;
            } else if (newWeight < -10000) {
                newWeight = -10000;
            }
            GuardUtil.isFinite(newWeight, "Connection weight is not finite");
            connection.setWeight(newWeight);
        }
    }
}
