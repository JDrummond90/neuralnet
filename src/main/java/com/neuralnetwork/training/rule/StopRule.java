package com.neuralnetwork.training.rule;

public interface StopRule {
    // todo inverse the naming/rules
    boolean canProceed(double currentError, int iterations);

    String getStopMessage();
}
