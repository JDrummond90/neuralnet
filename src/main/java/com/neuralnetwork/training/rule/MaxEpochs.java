package com.neuralnetwork.training.rule;

public class MaxEpochs implements StopRule {

    private final int maxEpochs;

    public MaxEpochs(int maxEpochs) {
        this.maxEpochs = maxEpochs;
    }

    @Override
    public boolean canProceed(double currentError, int epochs) {
        return epochs < maxEpochs;
    }

    @Override
    public String getStopMessage() {
        return "Max iterations reached";
    }
}
