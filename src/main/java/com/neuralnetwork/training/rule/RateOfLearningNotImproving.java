package com.neuralnetwork.training.rule;

/**
 * Exponentially weights the rate of change to determine if the network error is not changing
 */
public class RateOfLearningNotImproving implements StopRule {

    private final double SMA_ITERATIONS = 10;

    private final double minChange;
    private final double decay;

    private double ewma;
    private double priorError;

    /**
     * Indicates if the rate of change in the error condition is failing to improve
     *
     * @param minChange positive value indicating the smallest change in the exponential weight before stopping
     * @param decay     multiplier to the moving average, the latest change is multiplied by 1-decay
     */
    public RateOfLearningNotImproving(final double minChange, final double decay) {
        this.minChange = minChange;
        this.decay = decay;
    }

    @Override
    public boolean canProceed(double currentError, int epochs) {
        final double change = currentError - priorError;
        priorError = currentError;

        if(epochs == 1) {
            return Math.abs(change) > minChange;
        } else if (epochs <= SMA_ITERATIONS + 1) {
            ewma += change;
            if (epochs == SMA_ITERATIONS + 1) {
                ewma = ewma / epochs;
            } else {
                return (Math.abs(ewma) / epochs) > minChange;
            }
        } else {
            ewma = change * (1 - decay) + ewma * decay;
        }

        return Math.abs(ewma) > minChange;
    }

    @Override
    public String getStopMessage() {
        return "Rate of learning not improving";
    }
}
