package com.neuralnetwork.training;

import java.util.Arrays;

/**
 * Created by jamie on 08/01/18.
 */
public class DataRow {

    private final double[] input;
    private final double[] expectedOutput;

    public DataRow(double[] input, double[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    public double[] getInput() {
        return input;
    }

    public double[] getExpectedOutput() {
        return expectedOutput;
    }

    @Override
    public String toString() {
        return "DataRow{" +
                "input=" + Arrays.toString(input) +
                ", expectedOutput=" + Arrays.toString(expectedOutput) +
                '}';
    }
}
