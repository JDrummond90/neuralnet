package com.neuralnetwork.training;

import java.util.List;

/**
 * Created by jamie on 08/01/18.
 */
public class DataSet {

    private final List<DataRow> rows;

    public DataSet(List<DataRow> rows) {
        this.rows = rows;
    }

    public List<DataRow> getRows() {
        return rows;
    }
}
