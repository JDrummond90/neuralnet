package com.neuralnetwork.training.error;

/**
 * Created by jamie on 08/01/18.
 */
public interface ErrorFunction {

    double[] errorPerOutput(double[] networkOutput, double[] expected);

    double totalError();

    void startEpoch();
}
