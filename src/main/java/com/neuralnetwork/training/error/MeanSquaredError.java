package com.neuralnetwork.training.error;

/**
 * Created by jamie on 08/01/18.
 */
public class MeanSquaredError implements ErrorFunction {

    private double totalError;
    private double iterations;
    private final double[] error;

    public MeanSquaredError(int outputNeurons) {
        error = new double[outputNeurons];
    }

    @Override
    public void startEpoch() {
        totalError = 0;
        iterations = 0;
    }

    @Override
    public double[] errorPerOutput(double[] networkOutput, double[] expected) {
        // TODO this class is calculating MSE used to determine the models fitness and also
        // (output - expected) used to update weights
        double outputTotalError = 0;
        for (int i = 0; i < networkOutput.length; i++) {
            double outputError = networkOutput[i] - expected[i];
            double squaredError = Math.pow(outputError, 2);
            error[i] = outputError;//squaredError;
            outputTotalError += squaredError;
            iterations++;
        }
        totalError += outputTotalError / 2;
        return error;
    }

    @Override
    public double totalError() {
        return totalError ;/// iterations;
    }
}
