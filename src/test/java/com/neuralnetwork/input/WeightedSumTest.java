package com.neuralnetwork.input;

import com.neuralnetwork.Connection;
import com.neuralnetwork.InputNeuron;
import com.neuralnetwork.Neuron;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WeightedSumTest {

    @Test
    public void weightedInputs() {
        WeightedSum weightedSum = new WeightedSum();

        Neuron one = getNeuron(0.6);
        Neuron two = getNeuron(0.8);
        Neuron three = getNeuron(0.1);

        List<Connection> inputConnections = new ArrayList<>();
        inputConnections.add(new Connection(one, two, 0.5));
        inputConnections.add(new Connection(one, three, 0.3));

        assertEquals(0.48, weightedSum.calculateInput(inputConnections), 1e-10);
    }

    @NotNull
    private Neuron getNeuron(final double output) {
        return new InputNeuron() {
            @Override
            public double getOutput() {
                return output;
            }
        };
    }
}