package com.neuralnetwork;


import com.neuralnetwork.activation.LeakyRectifiedLinearActivationFunction;
import com.neuralnetwork.activation.LinearActivationFunction;
import com.neuralnetwork.activation.RectifiedLinearActivationFunction;
import com.neuralnetwork.activation.TanhActivationFunction;
import com.neuralnetwork.input.WeightedSum;
import com.neuralnetwork.training.BackPropagation;
import com.neuralnetwork.training.DataRow;
import com.neuralnetwork.training.DataSet;
import com.neuralnetwork.training.error.MeanSquaredError;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by jamie on 08/01/18.
 */
@Ignore
public class NeuralNetworkTest {

    @Test
    public void unitAreaCircle() {
        Network.NetworkBuilder networkBuilder = Network.builder();
        networkBuilder.addInputLayer(1, true);
        //networkBuilder.addHiddenLayer(10, true, new TanhActivationFunction(), new WeightedSum(), new RandomWeightInitialiser());
        networkBuilder.addHiddenLayer(10, true, new TanhActivationFunction(), new WeightedSum(), new GaussianWeightInitialiser(0.1, 0.01));
        //networkBuilder.addHiddenLayer(10, true, new LeakyRectifiedLinearActivationFunction(), new WeightedSum(), new GaussianWeightInitialiser(0.1, 0.01));
        networkBuilder.addOutputLayer(1, new LinearActivationFunction(), new WeightedSum(), new GaussianWeightInitialiser(0.1, 0.01));
        //addSoftmaxOutputLayer(networkBuilder);
        //network.addLayers(new TanhActivationFunction(), new WeightedSum(), true, 2, 3, 1);

        Network network = networkBuilder.build();

        List<DataRow> rows = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            double r = Math.random();
            double area = Math.PI * Math.pow(r, 2);
            rows.add(new DataRow(new double[]{ r }, new double[]{ area }));
            //rows.add(new DataRow(new double[]{area}, new double[]{r}));
        }
        DataSet dataSet = new DataSet(rows);

        double maxError = 0.00001;
        BackPropagation backPropagation = new BackPropagation(network, new MeanSquaredError(1),  0.1d, maxError);

        backPropagation.withDefaultRules();
        printNeuronMap(network);
        backPropagation.learn(dataSet);
        printNeuronMap(network);

        validate(rows, maxError, network);
    }

    @Test
    public void runXor() {

        double[][] xorInput = {{0, 0}, {1, 0}, {0, 1}, {1, 1}};
        double[][] xorOutput = {{0}, {1}, {1}, {0}};

        Network.NetworkBuilder networkBuilder = Network.builder();
        networkBuilder.addInputLayer(2, true);
        networkBuilder.addHiddenLayer(3, true, new TanhActivationFunction(), new WeightedSum(), new RandomWeightInitialiser());
        //networkBuilder.addOutputLayer(1, new TanhActivationFunction(), new WeightedSum(), hidden);
        addSoftmaxOutputLayer(networkBuilder);
        //network.addLayers(new TanhActivationFunction(), new WeightedSum(), true, 2, 3, 1);

        Network network = networkBuilder.build();
        List<DataRow> rows = new ArrayList<>();
        for (int i = 0; i < xorInput.length; i++) {
            DataRow row = new DataRow(xorInput[i], xorOutput[i]);
            rows.add(row);
        }
        DataSet dataSet = new DataSet(rows);

        double maxError = 0.01;
        BackPropagation backPropagation = new BackPropagation(network, new MeanSquaredError(1),
                0.4d, maxError);
        backPropagation.withDefaultRules();
        printNeuronMap(network);
        backPropagation.learn(dataSet);
        printNeuronMap(network);

        validate(rows, maxError, network);
    }

    private void validate(List<DataRow> rows, double maxError, Network network) {
        for (int i = 0; i < rows.size(); i++) {
            DataRow row = rows.get(i);
            double[] calculate = network.calculate(row.getInput());
            System.out.println("Expected " + row.getExpectedOutput()[0] + " actual " + calculate[0]);
            assertEquals("Failed on " + i  + ": " + row.toString(), row.getExpectedOutput()[0], calculate[0], maxError * 10);
        }
    }

    private void addSoftmaxOutputLayer(Network.NetworkBuilder networkBuilder) {
        SoftmaxLayer outputLayer = new SoftmaxLayer(networkBuilder.createNeurons(1, false, new LinearActivationFunction(), new WeightedSum()));
        networkBuilder.wireInNewLayer(outputLayer, new RandomWeightInitialiser());
    }

    private void printNeuronMap(Network network) {
        Map<Neuron, Integer> neuronIds = new HashMap<>();
        final int[] nextId = {1};

        for (Layer layer : network.getAllLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                neuronIds.put(neuron, nextId[0]++);
            }
        }

        for (Layer layer : network.getAllLayers()) {
            StringBuilder header = new StringBuilder();
            StringBuilder in = new StringBuilder();
            System.out.println("Layer: " + layer.getClass().toString());
            for (Neuron neuron : layer.getNeurons()) {
                String neuronKey = getKey(neuronIds, neuron);
                header.append("Neurons: ").append(neuronKey).append("\t");
                for (Connection connection : neuron.getOutputConnections()) {
                    in.append("Connections: [").append(getKey(neuronIds, connection.getFrom())).append(" -> ").append(neuronKey).append("\t").append(connection.getWeight()).append("] ");
                }
            }
            System.out.println(header.toString());
            System.out.println(in.toString());
        }
    }

    private String getKey(Map<Neuron, Integer> neuronIds, Neuron neuron) {
        return "N" + (neuron instanceof BiasNeuron ? "b" : "") + neuronIds.get(neuron);
    }
}