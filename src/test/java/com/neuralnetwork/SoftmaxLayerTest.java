package com.neuralnetwork;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SoftmaxLayerTest {

    @Test
    public void calculate() {
        final double[] input = new double[]{1, 2, 3, 4, 1, 2, 3};
        final double[] expected = new double[]{0.024, 0.064, 0.175, 0.475, 0.024, 0.064, 0.175};

        List<Neuron> neurons = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            int finalI = i;
            neurons.add(new AutoNeuron(null, null) {
                @Override
                public double getInput() {
                    return input[finalI];
                }

                @Override
                public void calculateInput() {
                }
            });
        }

        SoftmaxLayer layer = new SoftmaxLayer(neurons);
        layer.calculate();

        for (int i = 0; i < neurons.size(); i++) {
            assertEquals(expected[i], neurons.get(i).getOutput(), 1e-3);
        }
    }
}