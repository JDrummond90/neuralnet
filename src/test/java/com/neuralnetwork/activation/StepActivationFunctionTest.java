package com.neuralnetwork.activation;

import org.junit.Test;

import static org.junit.Assert.*;

public class StepActivationFunctionTest {

    @Test
    public void activate() {
        StepActivationFunction stepActivationFunction = new StepActivationFunction();
        assertEquals(0, stepActivationFunction.activate(0.4), 1e-6);
        assertEquals(1, stepActivationFunction.activate(0.55), 1e-6);
    }

    @Test
    public void derivative() {
        StepActivationFunction stepActivationFunction = new StepActivationFunction();
        assertEquals(1, stepActivationFunction.derivative(0.4), 1e-6);
        assertEquals(1, stepActivationFunction.derivative(0.55), 1e-6);
    }
}