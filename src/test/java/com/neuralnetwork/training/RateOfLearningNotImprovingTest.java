package com.neuralnetwork.training;

import com.neuralnetwork.training.rule.RateOfLearningNotImproving;
import org.junit.Test;

import static org.junit.Assert.*;

public class RateOfLearningNotImprovingTest {

    @Test
    public void shouldStopOnceConverged() {
        double initialRateOfChange = (1 / 1000d);
        RateOfLearningNotImproving rateOfLearningNotImproving = new RateOfLearningNotImproving(initialRateOfChange / 10, 0.95);

        // Generate data that initially linearly decreases before flattening out
        double[] inputs = new double[1000];
        for (int i = 0; i < 100; i++) {
            inputs[i] = 1 - initialRateOfChange * i;
        }

        for (int i = 0; i < 100; i++) {
            inputs[i + 100] = inputs[99];
        }

        int stoppedAt = 0;
        for (int i = 1; i <= 200; i++) {
            if(!rateOfLearningNotImproving.canProceed(inputs[i], i)){
                stoppedAt = i;
                break;
            }
        }

        assertEquals(144, stoppedAt);
    }
}