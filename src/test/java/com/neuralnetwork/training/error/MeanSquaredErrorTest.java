package com.neuralnetwork.training.error;

import org.junit.Test;

import static org.junit.Assert.*;

public class MeanSquaredErrorTest {

    @Test
    public void calculateError() {

        double[] output = { 0, 0.4, 0.5, 1};
        double[] expected = { 0.1, 0, 0.7, 1};

        MeanSquaredError errorFunction = new MeanSquaredError(4);
        double[] error = errorFunction.errorPerOutput(output, expected);

        assertEquals(-0.1, error[0], 1e-6);
        assertEquals(0.4, error[1], 1e-6);
        assertEquals(-0.2, error[2], 1e-6);
        assertEquals(0, error[3], 1e-6);
    }
}