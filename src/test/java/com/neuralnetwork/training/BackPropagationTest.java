package com.neuralnetwork.training;

import com.neuralnetwork.Layer;
import com.neuralnetwork.Network;
import com.neuralnetwork.Neuron;
import com.neuralnetwork.WeightInitialiser;
import com.neuralnetwork.activation.SigmoidActivationFunction;
import com.neuralnetwork.input.WeightedSum;
import com.neuralnetwork.training.error.MeanSquaredError;
import com.neuralnetwork.training.rule.MaxEpochs;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by jamie on 08/01/18.
 */
public class BackPropagationTest {

    /**
     * With a 2,2,2 network correctly apply error
     *
     * @throws Exception
     */
    @Test
    public void learn() throws Exception {
        Network.NetworkBuilder networkBuilder = Network.builder();

        networkBuilder.addInputLayer(2, true);
        double[] hiddenWeights = new double[]{0.15, 0.2, 0.35, 0.25, 0.3, 0.35};
        networkBuilder.addHiddenLayer(2, true, new SigmoidActivationFunction(), new WeightedSum(), fixedWeightInitialiser(hiddenWeights));

        double[] outputWeights = new double[]{0.4, 0.45, 0.6, 0.5, 0.55, 0.6};
        networkBuilder.addOutputLayer(2, new SigmoidActivationFunction(), new WeightedSum(), fixedWeightInitialiser(outputWeights));

        Network network = networkBuilder.build();
        BackPropagation backPropagation = new BackPropagation(network, new MeanSquaredError(2), 0.5, 0.01);
        backPropagation.addRule(new MaxEpochs(1));

        DataRow row = new DataRow(new double[]{0.05, 0.1}, new double[]{0.01, 0.99});
        DataSet dataSet = new DataSet(Arrays.asList(row));

        backPropagation.learn(dataSet);

        double[] expectedOutputWeights = new double[] { 0.3589, 0.4086, 0.5307, 0.5113, 0.5613, 0.6190 };
        assertWeights(expectedOutputWeights, network.getOutputLayer());
        double[] expectedHiddenWeights = new double[] { 0.1499, 0.1996, 0.3463, 0.2497, 0.2995, 0.3457 };
        assertWeights(expectedHiddenWeights, network.getHiddenLayers().get(0));
    }

    private void assertWeights(double[] expectedOutputWeights, Layer layer) {
        int index = 0;
        for (Neuron neuron : layer.getNeurons()) {
            for (int i = 0; i < neuron.getInputConnections().size(); i++) {
                assertEquals(expectedOutputWeights[index++], neuron.getInputConnections().get(i).getWeight(), 1e-4);
            }
        }
    }

    @NotNull
    private WeightInitialiser fixedWeightInitialiser(double[] outputWeights) {
        return neurons -> {
            int index = 0;
            for (Neuron neuron : neurons) {
                for (int i = 0; i < neuron.getInputConnections().size(); i++) {
                    neuron.getInputConnections().get(i).setWeight(outputWeights[index++]);
                }
            }
        };
    }

}