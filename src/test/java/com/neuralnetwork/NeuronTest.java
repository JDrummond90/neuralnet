package com.neuralnetwork;

import com.neuralnetwork.activation.StepActivationFunction;
import com.neuralnetwork.input.WeightedSum;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jamie on 08/01/18.
 */
public class NeuronTest {

    @Test
    public void testCalculate() {
        Neuron one = new InputNeuron() {
            @Override
            public double getOutput() {
                return 0.6;
            }
        };
        Neuron two = new InputNeuron() {
            @Override
            public double getOutput() {
                return 0.8;
            }
        };
        Neuron three = new AutoNeuron(new StepActivationFunction(), new WeightedSum());
        Neuron four = new AutoNeuron(new StepActivationFunction(), new WeightedSum());

        Connection.addIfMissing(one, three, 0.9);
        Connection.addIfMissing(one, four, 0.6);
        Connection.addIfMissing(two, three, 0.7);
        Connection.addIfMissing(two, four, 0.8);

        three.calculateInput();
        three.calculateOutput();
        assertEquals(1, three.getOutput(), 1e-6);
    }
}