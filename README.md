# Simple neural network
A very simple neural network that I used as an aid when learning about neural networks.

Should not be used in any capacity other than for learning about the basic implementation of a neural network as it is missing key features such as batch updates, a suitable variety of optimisers, GPU support/better matrices for weights/parameters and thorough validation/testing. Tensorflow or Deeplearning4j both have these required features and much more
